// © 2022 Christoph Grenz <https://grenz-bonn.de>
//
// SPDX-License-Identifier: MPL-2.0

use core::fmt;
use core::ops::Deref;
use core::str::FromStr;
#[cfg(feature = "std")]
use std::io;

use crate::category::Category;
use crate::character::Char;
use crate::class::Class;
use crate::error::ParseError;

/// Representation for an alphanumeric SQLSTATE code.
///
/// This struct can be treated like a 5-byte fixed-length string restricted to `A`-`Z` and `0`-`9`.
/// It is dereferencable as a [`str`] and can be converted into `[u8; 5]` or `&[u8]` using [`Into`]
/// and [`AsRef`].
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct SqlState {
	code: [Char; 5],
}

impl SqlState {
	/// Creates an `SqlState` from a string.
	///
	/// # Errors
	/// Returns an `Err` if the string isn't of length 5 or contains characters other than `A`-`Z`
	/// and `0`-`9`.
	///
	/// # Examples
	/// ```
	/// # use sqlstate_inline::{SqlState, ParseError};
	/// # fn main() -> Result<(), ParseError> {
	/// let input = "08001";
	/// let sqlstate = SqlState::from_str(input)?;
	///
	/// assert_eq!(input, &*sqlstate);
	/// # Ok(())
	/// # }
	/// ```
	#[inline]
	pub const fn from_str(value: &str) -> Result<Self, ParseError> {
		Self::from_bytes(value.as_bytes())
	}

	/// Creates an `SqlState` from a string, replacing invalid characters.
	///
	/// SQLSTATEs are strings of length 5 containing only ASCII characters `A`-`Z` and `0`-`9`.
	/// The first two characters designate the "class" and the remaining three characters designate
	/// the "subclass". This constructor truncates longer strings, and replaces missing or invalid
	/// characters by the following rules:
	///
	/// - an empty string is always replaced with [`SqlState::UNKNOWN`].
	/// - else every missing or invalid character is replaced with `X`.
	///
	/// # Examples
	/// ```
	/// # use sqlstate_inline::{SqlState, ParseError};
	/// let input = "080@Ä";
	/// let sqlstate = SqlState::from_str_lossy(input);
	///
	/// assert_eq!(SqlState("080XX"), sqlstate);
	/// ```
	#[inline]
	pub const fn from_str_lossy(value: &str) -> Self {
		Self::from_bytes_lossy(value.as_bytes())
	}

	/// Creates an `SqlState` from an ASCII byte string.
	///
	/// # Errors
	/// Returns an `Err` if the slice isn't of length 5 or contains ASCII character codes other
	/// than `A`-`Z` and `0`-`9`.
	///
	/// # Examples
	/// ```
	/// # use sqlstate_inline::{SqlState, ParseError};
	/// # fn main() -> Result<(), ParseError> {
	/// let input = b"08001";
	/// let sqlstate = SqlState::from_bytes(input)?;
	///
	/// assert_eq!(input, AsRef::<[u8]>::as_ref(&sqlstate));
	/// # Ok(())
	/// # }
	/// ```
	#[inline]
	pub const fn from_bytes(value: &[u8]) -> Result<Self, ParseError> {
		match Char::new_array(value) {
			Ok(code) => Ok(Self { code }),
			Err(e) => Err(e),
		}
	}

	/// Creates an `SqlState` from a byte string, replacing invalid characters.
	///
	/// This is the ASCII byte string version of [`SqlState::from_str_lossy()`].
	#[inline]
	pub const fn from_bytes_lossy(value: &[u8]) -> Self {
		if value.is_empty() {
			Self::UNKNOWN
		} else {
			Self {
				code: Char::new_array_lossy(value, Char::X),
			}
		}
	}

	/// Creates an `SqlState` from an ASCII byte array.
	///
	/// # Errors
	/// Returns an `Err` if the array contains ASCII character codes other than `A`-`Z` and `0`-`9`.
	///
	/// # Examples
	/// ```
	/// # use sqlstate_inline::{SqlState, ParseError};
	/// # fn main() -> Result<(), ParseError> {
	/// let input = [b'4', b'2', b'0', b'0', b'0'];
	/// let sqlstate = SqlState::from_byte_array(input)?;
	///
	/// assert_eq!(&input, AsRef::<[u8; 5]>::as_ref(&sqlstate));
	/// # Ok(())
	/// # }
	/// ```
	///
	/// # See also
	/// [`Class::from_bytes()`]
	pub const fn from_byte_array(value: [u8; 5]) -> Result<Self, ParseError> {
		match Char::new_array(&value) {
			Ok(code) => Ok(Self { code }),
			Err(e) => Err(e),
		}
	}

	/// Returns the general category for this SQLSTATE.
	///
	/// # Examples
	/// ```
	/// # use sqlstate_inline::{SqlState, Category};
	/// let sqlstate = SqlState("08001");
	/// assert_eq!(sqlstate.category(), Category::Exception);
	/// ```
	#[inline]
	pub const fn category(&self) -> Category {
		match self {
			sqlstate![0 0 ..] => Category::Success,
			sqlstate![0 1 ..] => Category::Warning,
			sqlstate![0 2 ..] => Category::NoData,
			_ => Category::Exception,
		}
	}

	/// Returns the class code for this SQLSTATE.
	/// ```
	/// # use sqlstate_inline::{SqlState, Class};
	/// let sqlstate = SqlState("42000");
	/// let class = sqlstate.class();
	///
	/// assert_eq!(class, Class::SYNTAX_ERROR_OR_ACCESS_RULE_VIOLATION);
	/// assert_eq!(&class, "42");
	/// ```
	#[inline]
	pub const fn class(&self) -> Class {
		match self.code {
			[a, b, _, _, _] => Class { code: [a, b] },
		}
	}

	/// Returns whether this code is implementation-specific and not reserved for standard conditions.
	///
	/// All classes and all subclasses starting with `5`-`9` or `I`-`Z` are implementation-specific.
	///
	/// # Examples
	/// ```
	/// # use sqlstate_inline::{SqlState, Category};
	/// let sqlstate = SqlState("42000");
	/// assert_eq!(sqlstate.is_implementation_specific(), false);
	///
	/// let sqlstate = SqlState("XRUST");
	/// assert_eq!(sqlstate.is_implementation_specific(), true);
	/// let sqlstate = SqlState("XR000");
	/// assert_eq!(sqlstate.is_implementation_specific(), true);
	/// let sqlstate = SqlState("42R42");
	/// assert_eq!(sqlstate.is_implementation_specific(), true);
	/// ```
	#[inline]
	pub const fn is_implementation_specific(&self) -> bool {
		!matches!(
			self.code[0],
			Char::_0
				| Char::_1 | Char::_2
				| Char::_3 | Char::_4
				| Char::A | Char::B
				| Char::C | Char::D
				| Char::E | Char::F
				| Char::G | Char::H
		) || !matches!(
			self.code[2],
			Char::_0
				| Char::_1 | Char::_2
				| Char::_3 | Char::_4
				| Char::A | Char::B
				| Char::C | Char::D
				| Char::E | Char::F
				| Char::G | Char::H
		)
	}

	/// Returns whether the SQLSTATE contains a subclass.
	///
	/// `false` if the code is a generic class code ending in `000`.
	#[inline]
	pub const fn has_subclass(&self) -> bool {
		!matches!(self.code, [_, _, Char::_0, Char::_0, Char::_0])
	}

	/// `SqlState("99999")`
	///
	/// Used by Oracle for uncategorized errors and also used in this crate
	/// when an empty string is passed to [`SqlState::from_str_lossy()`].
	pub const UNKNOWN: Self = sqlstate![9 9 9 9 9];
}

impl fmt::Debug for SqlState {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
		f.debug_tuple("SqlState").field(&&**self).finish()
	}
}

impl fmt::Display for SqlState {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
		f.write_str(self)
	}
}

impl AsRef<str> for SqlState {
	#[inline]
	fn as_ref(&self) -> &str {
		self
	}
}

impl AsRef<[u8]> for SqlState {
	#[inline]
	fn as_ref(&self) -> &[u8] {
		Char::array_as_bytes(&self.code)
	}
}

impl AsRef<[u8; 5]> for SqlState {
	#[inline]
	fn as_ref(&self) -> &[u8; 5] {
		Char::array_as_bytes(&self.code)
	}
}

impl From<SqlState> for [u8; 5] {
	#[inline]
	fn from(sqlstate: SqlState) -> Self {
		sqlstate.code.map(Char::as_byte)
	}
}

impl Deref for SqlState {
	type Target = str;
	#[inline]
	fn deref(&self) -> &Self::Target {
		Char::array_as_str(&self.code)
	}
}

impl PartialEq<str> for SqlState {
	fn eq(&self, other: &str) -> bool {
		&**self == other
	}
}

impl PartialEq<[u8]> for SqlState {
	fn eq(&self, other: &[u8]) -> bool {
		AsRef::<[u8]>::as_ref(self) == other
	}
}

#[cfg(feature = "std")]
impl From<SqlState> for io::ErrorKind {
	/// Converts the SQLSTATE to an [`std::io::ErrorKind`].
	///
	/// Convert well-known codes that have a rough equivalent to the corresponding `ErrorKind` on
	/// a best-effort basis and falls back to [`ErrorKind::Other`] for everything else. Thus this
	/// mapping is not considered stable and might change once new `ErrorKind` variants are added.
	///
	/// [`ErrorKind::Other`]: `io::ErrorKind::Other`
	fn from(state: SqlState) -> Self {
		match state {
			// 08: Connection errors
			sqlstate![0 8 0 0 1] | sqlstate![0 8 0 0 4] => Self::ConnectionRefused,
			sqlstate![0 8 0 0 3] => Self::ConnectionAborted,
			sqlstate![0 8 ..] => Self::ConnectionReset,

			// 0A: Unsupported
			sqlstate![0 A ..] => Self::Unsupported,

			// 21: Cardinality error
			sqlstate![2 1 ..] => Self::InvalidInput,

			// 22: Query data errors
			sqlstate![2 2 ..] => Self::InvalidData,

			// 23: Constraint violation
			sqlstate![2 3 5 0 5] => Self::AlreadyExists,
			sqlstate![2 3 ..] => Self::PermissionDenied,

			// 25: Invalid transaction state
			//sqlstate![2 5 0 0 6] => Self::ReadOnlyFilesystem,
			sqlstate![2 5 P 0 3] => Self::TimedOut,

			// 26: Invalid statement name
			sqlstate![2 6 ..] => Self::InvalidData,

			// 28: Authorization
			sqlstate![2 8 ..] => Self::PermissionDenied,

			// 40: Transaction rollback
			//sqlstate![4 0 0 0 1] => Self::ResourceBusy,
			sqlstate![4 0 0 0 2] => Self::PermissionDenied,
			//sqlstate![4 0 P 0 1] => Self::Deadlock,

			// 42: Query syntax error or access rule violation
			sqlstate![4 2 5 ..] => Self::PermissionDenied,
			sqlstate![4 2 S 0 1] => Self::AlreadyExists,
			sqlstate![4 2 S 0 2] | sqlstate![4 2 S 1 2] => Self::NotFound,
			sqlstate![4 2 ..] => Self::InvalidInput,

			// 44: WITH CHECK OPTION violation
			sqlstate![4 4 ..] => Self::PermissionDenied,

			// 53 (PostgreSQL): Resource exhaustion
			//sqlstate![5 3 1 0 0] => Self::StorageFull,
			sqlstate![5 3 2 0 0] => Self::OutOfMemory,
			sqlstate![5 3 3 0 0] => Self::ConnectionRefused,

			// 54 (PostgreSQL): Program limit exceeded
			sqlstate![5 4 ..] => Self::InvalidInput,

			// 57 (PostgreSQL): Intervention
			sqlstate![5 7 0 1 4] => Self::Interrupted,
			sqlstate![5 7 P 0 1] => Self::ConnectionReset,
			sqlstate![5 7 P 0 2] => Self::ConnectionReset,
			sqlstate![5 7 P 0 3] => Self::ConnectionRefused,
			//sqlstate![5 7 P 0 4] => Self::StaleNetworkFileHandle,
			sqlstate![5 7 P 0 5] => Self::ConnectionAborted,

			// 70 (MySQL): Interruption
			sqlstate![7 0 1 0 0] => Self::Interrupted,

			// HV: Foreign data wrappers
			sqlstate![H V 0 0 1] => Self::OutOfMemory,
			//sqlstate![H V 0 0 B] => Self::StaleNetworkFileHandle,
			sqlstate![H V 0 0 N] => Self::BrokenPipe,

			// HY: Call level interface
			sqlstate![H Y 0 0 1] => Self::OutOfMemory,
			sqlstate![H Y 0 0 8] => Self::TimedOut,

			// XX (PostgreSQL): Internal Error / Data corruption
			sqlstate![X X 0 0 1] | sqlstate![X X 0 0 2] => Self::UnexpectedEof,

			_ => Self::Other,
		}
	}
}

impl FromStr for SqlState {
	type Err = ParseError;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		Self::from_bytes(s.as_bytes())
	}
}

impl TryFrom<&[u8]> for SqlState {
	type Error = ParseError;

	fn try_from(bytes: &[u8]) -> Result<Self, Self::Error> {
		Self::from_bytes(bytes)
	}
}

impl TryFrom<[u8; 5]> for SqlState {
	type Error = ParseError;

	fn try_from(bytes: [u8; 5]) -> Result<Self, Self::Error> {
		Self::from_byte_array(bytes)
	}
}

impl TryFrom<&str> for SqlState {
	type Error = ParseError;

	fn try_from(string: &str) -> Result<Self, Self::Error> {
		Self::from_str(string)
	}
}

#[cfg(feature = "serde")]
impl serde::Serialize for SqlState {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: serde::Serializer,
	{
		serializer.serialize_str(self)
	}
}

#[cfg(feature = "serde")]
impl<'de> serde::Deserialize<'de> for SqlState {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: serde::Deserializer<'de>,
	{
		deserializer
			.deserialize_str(crate::character::de::ArrayVisitor::new())
			.map(|arr| Self { code: arr })
	}
}

// Statically assert the intended memory layouts (5 bytes with multiple niches)
const _: () = assert!(core::mem::size_of::<SqlState>() == 5);
const _: () = assert!(core::mem::size_of::<Option<Option<SqlState>>>() == 5);
