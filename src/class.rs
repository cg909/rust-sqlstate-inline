// © 2022 Christoph Grenz <https://grenz-bonn.de>
//
// SPDX-License-Identifier: MPL-2.0

use core::fmt;
use core::ops::Deref;
use core::str::FromStr;

use crate::category::Category;
use crate::character::Char;
use crate::error::ParseError;
use crate::sqlstate::SqlState;

/// Class code for a given SQLSTATE code.
///
/// The class consists of the first two characters of an SQLSTATE code.
///
/// This struct can be treated like a 2-byte fixed-length string restricted to `A`-`Z` and `0`-`9`.
/// It is dereferencable as a [`str`] and may be converted into a `[u8; 2]` or `&[u8]` using
/// [`Into`] and [`AsRef`].
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Class {
	pub(crate) code: [Char; 2],
}

impl Class {
	/// Creates a `Class` from a string.
	///
	/// # Errors
	/// Returns an `Err` if the string isn't of length 2 or contains characters other than `A`-`Z`
	/// and `0`-`9`.
	///
	/// # Examples
	/// ```
	/// # use sqlstate_inline::{Class, ParseError};
	/// # fn main() -> Result<(), ParseError> {
	/// let input = "08";
	/// let class = Class::from_str(input)?;
	///
	/// assert_eq!(input, &*class);
	/// # Ok(())
	/// # }
	/// ```
	#[inline]
	pub const fn from_str(value: &str) -> Result<Self, ParseError> {
		Self::from_bytes(value.as_bytes())
	}

	/// Creates a `Class` from an ASCII byte string.
	///
	/// # Errors
	/// Returns an `Err` if the slice isn't of length 2 or contains ASCII character codes other
	/// than `A`-`Z` and `0`-`9`.
	///
	/// # Examples
	/// ```
	/// # use sqlstate_inline::{Class, ParseError};
	/// # fn main() -> Result<(), ParseError> {
	/// let input = b"08";
	/// let class = Class::from_bytes(input)?;
	///
	/// assert_eq!(input, AsRef::<[u8]>::as_ref(&class));
	/// # Ok(())
	/// # }
	/// ```
	#[inline]
	pub const fn from_bytes(value: &[u8]) -> Result<Self, ParseError> {
		match Char::new_array(value) {
			Ok(code) => Ok(Self { code }),
			Err(e) => Err(e),
		}
	}

	/// Creates a `Class` from an ASCII byte array.
	///
	/// # Errors
	/// Returns an `Err` if the array contains ASCII character codes other than `A`-`Z` and `0`-`9`.
	///
	/// # Examples
	/// ```
	/// # use sqlstate_inline::{Class, ParseError};
	/// # fn main() -> Result<(), ParseError> {
	/// let input = [b'2', b'4'];
	/// let class = Class::from_byte_array(input)?;
	///
	/// assert_eq!(&input, AsRef::<[u8; 2]>::as_ref(&class));
	/// # Ok(())
	/// # }
	/// ```
	///
	/// # See also
	/// [`Class::from_bytes()`]
	#[inline]
	pub const fn from_byte_array(value: [u8; 2]) -> Result<Self, ParseError> {
		match Char::new_array(&value) {
			Ok(code) => Ok(Self { code }),
			Err(e) => Err(e),
		}
	}

	/// Returns the general category for this code class.
	///
	/// # Examples
	/// ```
	/// # use sqlstate_inline::{Class, Category};
	/// let class = Class::CONNECTION_EXCEPTION;
	/// assert_eq!(class.category(), Category::Exception);
	/// ```
	#[inline]
	pub const fn category(&self) -> Category {
		match self {
			sqlstate![0 0] => Category::Success,
			sqlstate![0 1] => Category::Warning,
			sqlstate![0 2] => Category::NoData,
			_ => Category::Exception,
		}
	}

	/// Returns whether a SQLSTATE is in this class.
	///
	/// # Examples
	/// ```
	/// # use sqlstate_inline::{Class, SqlState};
	/// let connection_refused = SqlState("08001");
	/// let syntax_error = SqlState("42000");
	/// let class = Class::CONNECTION_EXCEPTION;
	///
	/// assert_eq!(class.contains(connection_refused), true);
	/// assert_eq!(class.contains(syntax_error), false);
	/// ```
	#[inline]
	pub const fn contains(&self, sqlstate: SqlState) -> bool {
		let class = sqlstate.class();
		let self_arrayref = Char::array_as_bytes(&self.code);
		let other_arrayref = Char::array_as_bytes(&class.code);
		self_arrayref[0] == other_arrayref[0] && self_arrayref[1] == other_arrayref[1]
	}

	/// Returns whether this class is implementation-specific and not reserved for standard conditions.
	///
	/// All classes starting with `5`-`9` or `I`-`Z` are implementation-specific.
	///
	/// # Examples
	/// ```
	/// # use sqlstate_inline::{Class, Category};
	/// let class = Class::CONNECTION_EXCEPTION;
	/// assert_eq!(class.is_implementation_specific(), false);
	///
	/// let class = Class::PGSQL_OPERATOR_INTERVENTION;
	/// assert_eq!(class.is_implementation_specific(), true);
	///
	/// let class = Class("XR");
	/// assert_eq!(class.is_implementation_specific(), true);
	/// ```
	#[inline]
	pub const fn is_implementation_specific(&self) -> bool {
		!matches!(
			self.code[0],
			Char::_0
				| Char::_1 | Char::_2
				| Char::_3 | Char::_4
				| Char::A | Char::B
				| Char::C | Char::D
				| Char::E | Char::F
				| Char::G | Char::H
		)
	}
}

/// # SQL standard class constants
impl Class {
	/// Class `00`: Successful completion
	pub const SUCCESSFUL_COMPLETION: Self = sqlstate![0 0];
	/// Class `01`: Warning
	pub const WARNING: Self = sqlstate![0 1];
	/// Class `02`: No Data
	pub const NO_DATA: Self = sqlstate![0 2];
	/// Class `07`: Dynamic SQL error
	pub const DYNAMIC_SQL_ERROR: Self = sqlstate![0 7];
	/// Class `08`: Connection exception
	pub const CONNECTION_EXCEPTION: Self = sqlstate![0 8];
	/// Class `09`: Triggered action exception
	pub const TRIGGERED_ACTION_EXCEPTION: Self = sqlstate![0 9];
	/// Class `0A`: Feature not supported
	pub const FEATURE_NOT_SUPPORTED: Self = sqlstate![0 A];
	/// Class `0D`: Invalid target type specification
	pub const INVALID_TARGET_TYPE_SPEC: Self = sqlstate![0 D];
	/// Class `0E`: Invalid schema name list specification
	pub const INVALID_SCHEMA_NAME_LIST_SPEC: Self = sqlstate![0 E];
	/// Class `0F`: Locator exception
	pub const LOCATOR_EXCEPTION: Self = sqlstate![0 F];
	/// Class `0K`: Resignal when handler not active (SQL/PSM)
	pub const RESIGNAL_WHEN_HANDLER_NOT_ACTIVE: Self = sqlstate![0 K];
	/// Class `0L`: Invalid grantor
	pub const INVALID_GRANTOR: Self = sqlstate![0 L];
	/// Class `0M`: Invalid SQL-invoked procedure reference
	pub const INVALID_SQL_INVOKED_PROCEDURE_REF: Self = sqlstate![0 M];
	/// Class `0N`: SQL/XML mapping error (SQL/XML)
	pub const SQL_XML_MAPPING_ERROR: Self = sqlstate![0 N];
	/// Class `0P`: Invalid role specification
	pub const INVALID_ROLE_SPEC: Self = sqlstate![0 P];
	/// Class `0S`: Invalid transform group name specification
	pub const INVALID_TRANSFORM_GROUP_NAME_SPEC: Self = sqlstate![0 S];
	/// Class `0T`: Target table disagrees with cursor specification
	pub const TARGET_TABLE_DISAGREES_WITH_CURSOR_SPEC: Self = sqlstate![0 T];
	/// Class `0U`: Attempt to assign to non-updatable column
	pub const ATTEMPT_TO_ASSIGN_TO_NON_UPDATABLE_COLUMN: Self = sqlstate![0 U];
	/// Class `0V`: Attempt to assign to ordering column
	pub const ATTEMPT_TO_ASSSIGN_TO_ORDERING_COLUMN: Self = sqlstate![0 V];
	/// Class `0W`: Prohibited statement encoutered during trigger execution
	pub const PROHIBITED_STATEMENT_DURING_TRIGGER_EXEC: Self = sqlstate![0 W];
	/// Class `0X`: Invalid foreign server specification (SQL/MED)
	pub const INVALID_FOREIGN_SERVER_SPEC: Self = sqlstate![0 X];
	/// Class `0Y`: Pass-through specific condition (SQL/MED)
	pub const PASSTHROUGH_SPECIFIC_CONDITION: Self = sqlstate![0 Y];
	/// Class `0Z`: Diagnostics exception
	pub const DIAGNOSTICS_EXCEPTION: Self = sqlstate![0 Z];
	/// Class `10`: XQuery error (SQL/XML)
	pub const XQUERY_ERROR: Self = sqlstate![1 0];
	/// Class `20`: Case not found for case statement (SQL/PSM)
	pub const CASE_NOT_FOUND_FOR_CASE_STATEMENT: Self = sqlstate![2 0];
	/// Class `21`: Cardinality violation
	///
	/// Subquery row count mismatch, column count mismatch, etc.
	pub const CARDINALITY_VIOLATION: Self = sqlstate![2 1];
	/// Class `22`: Data exception
	///
	/// Overflow, truncation, out of range, etc.
	pub const DATA_EXCEPTION: Self = sqlstate![2 2];
	/// Class `23`: Integrity constraint violation
	///
	/// `RESTRICT` violation, foreign key constraint violation, etc.
	pub const INTEGRITY_CONSTRAINT_VIOLATION: Self = sqlstate![2 3];
	/// Class `24`: Invalid cursor state
	pub const INVALID_CURSOR_STATE: Self = sqlstate![2 4];
	/// Class `25`: Invalid transaction state
	pub const INVALID_TRANSACTION_STATE: Self = sqlstate![2 5];
	/// Class `26`: Invalid SQL statement name
	pub const INVALID_SQL_STATEMENT_NAME: Self = sqlstate![2 6];
	/// Class `27`: Invalid SQL statement name
	pub const TRIGGERED_DATA_CHANGE_VIOLATION: Self = sqlstate![2 7];
	/// Class `28`: Invalid authorization specification
	pub const INVALID_AUTHORIZATION_SPEC: Self = sqlstate![2 8];
	/// Class `2B`: Dependent privilege descriptors still exist
	pub const DEPENDENT_PRIVILEGE_DESCRIPTORS_STILL_EXIST: Self = sqlstate![2 B];
	/// Class `2C`: Invalid character set name
	pub const INVALID_CHARACTER_SET_NAME: Self = sqlstate![2 C];
	/// Class `2D`: Invalid transaction termination
	pub const INVALID_TRANSACTION_TERMINATION: Self = sqlstate![2 D];
	/// Class `2E`: Invalid connection name
	pub const INVALID_CONNECTION_NAME: Self = sqlstate![2 E];
	/// Class `2F`: SQL routine exception
	pub const SQL_ROUTINE_EXCEPTION: Self = sqlstate![2 F];
	/// Class `2H`: Invalid collation name
	pub const INVALID_COLLATION_NAME: Self = sqlstate![2 H];
	/// Class `30`: Invalid SQL statement identifier
	pub const INVALID_SQL_STATEMENT_IDENTIFIER: Self = sqlstate![3 0];
	/// Class `33`: Invalid SQL descriptor name
	pub const INVALID_SQL_DESCRIPTOR_NAME: Self = sqlstate![3 3];
	/// Class `34`: Invalid cursor name
	pub const INVALID_CURSOR_NAME: Self = sqlstate![3 4];
	/// Class `35`: Invalid connection number
	pub const INVALID_CONNECTION_NUMBER: Self = sqlstate![3 5];
	/// Class `36`: Cursor sensitivity exception
	pub const CURSOR_SENSITIVITY_EXCEPTION: Self = sqlstate![3 6];
	/// Class `38`: External routine exception
	pub const EXTERNAL_ROUTINE_EXCEPTION: Self = sqlstate![3 8];
	/// Class `39`: External routine invocation exception
	pub const EXTERNAL_ROUTINE_INVOCATION_EXCEPTION: Self = sqlstate![3 9];
	/// Class `3B`: External routine invocation exception
	pub const SAVEPOINT_EXCEPTION: Self = sqlstate![3 B];
	/// Class `3C`: Ambiguous cursor name
	pub const AMBIGUOUS_CURSOR_NAME: Self = sqlstate![3 C];
	/// Class `3D`: Invalid catalog name
	///
	/// Empty or invalid database name, etc.
	pub const INVALID_CATALOG_NAME: Self = sqlstate![3 D];
	/// Class `3F`: Invalid schema name
	pub const INVALID_SCHEMA_NAME: Self = sqlstate![3 F];
	/// Class `40`: Transaction rollback
	pub const TRANSACTION_ROLLBACK: Self = sqlstate![4 0];
	/// Class `42`: Syntax error or access rule violation
	///
	/// Includes SQL query syntax errors, miscellaneous query errors, and access right violations.
	pub const SYNTAX_ERROR_OR_ACCESS_RULE_VIOLATION: Self = sqlstate![4 2];
	/// Class `44`: With check option violation
	pub const WITH_CHECK_OPTION_VIOLATION: Self = sqlstate![4 4];
	/// Class `45`: Unhandled user-defined exception (SQL/PSM)
	pub const UNHANDLED_USER_DEFINED_EXCEPTION: Self = sqlstate![4 5];
	/// Class `46`: OLB-specific error (SQL/OLB)
	///
	/// Includes Java DDL errors from SQL/JRT.
	pub const OLB_SPECIFIC_ERROR: Self = sqlstate![4 6];
	/// Class `HW`: Datalink exception (SQL/MED)
	pub const DATALINK_EXCEPTION: Self = sqlstate![H W];
	/// Class `HV`: FDW-specific condition (SQL/MED)
	pub const FDW_SPECIFIC_CONDITION: Self = sqlstate![H V];
	/// Class `HY`: CLI-specific condition (SQL/CLI)
	///
	/// CLI stands for "Call-Level Interface". This mainly subsumes API errors
	pub const CLI_SPECIFIC_CONDITION: Self = sqlstate![H Y];
}

/// # IBM DB2-specific class constants
impl Class {
	/// DB2-specific: Class `51`: Invalid application state
	pub const DB2_INVALID_APPLICATION_STATE: Self = sqlstate![5 1];
	/// DB2-specific: Class `53`: Invalid operand or inconsistent specification
	pub const DB2_INVALID_OPERAND_OR_INCONSISTENT_SPEC: Self = sqlstate![5 3];
	/// DB2-specific: Class `54`: SQL or product limit exceeded
	pub const DB2_SQL_OR_PRODUCT_LIMIT_EXCEEDED: Self = sqlstate![5 4];
	/// DB2-specific: Class `55`: Object not in prerequisite state
	pub const DB2_OBJECT_NOT_IN_PREREQUISITE_STATE: Self = sqlstate![5 5];
	/// DB2-specific: Class `56`: Miscellaneous SQL or product error
	pub const DB2_MISCELLANEOUS_SQL_OR_PRODUCT_ERROR: Self = sqlstate![5 6];
	/// DB2-specific: Class `57`: Resource not available or operator intervention
	pub const DB2_RESOURCE_NOT_AVAILABLE_OR_OPERATOR_INTERVENTION: Self = sqlstate![5 7];
	/// DB2-specific: Class `58`: System error
	pub const DB2_SYSTEM_ERROR: Self = sqlstate![5 8];
	/// DB2-specific: Class `5U`: Common utilities and tools
	pub const DB2_COMMON_UTILITIES_AND_TOOLS: Self = sqlstate![5 U];
}

/// # PostgreSQL-specific class constants
impl Class {
	/// Postgres-specific: Class `53`: Invalid operand or inconsistent specification
	pub const PGSQL_INSUFFICIENT_RESOURCES: Self = sqlstate![5 3];
	/// Postgres-specific: Class `54`: Program limit exceeded
	pub const PGSQL_PROGRAM_LIMIT_EXCEEDED: Self = sqlstate![5 4];
	/// Postgres-specific: Class `55`: Object not in prerequisite state
	pub const PGSQL_OBJECT_NOT_IN_PREREQUISITE_STATE: Self = sqlstate![5 5];
	/// Postgres-specific: Class `57`: Operator intervention
	pub const PGSQL_OPERATOR_INTERVENTION: Self = sqlstate![5 7];
	/// Postgres-specific: Class `58`: System error (errors external to PostgreSQL itself)
	pub const PGSQL_SYSTEM_ERROR: Self = sqlstate![5 8];
	/// Postgres-specific: Class `72`: Snapshot Failure
	pub const PGSQL_SNAPSHOT_FAILURE: Self = sqlstate![7 2];
	/// Postgres-specific: Class `F0`: Configuration File Error
	pub const PGSQL_CONFIGURATION_FILE_ERROR: Self = sqlstate![F 0];
	/// Postgres-specific: Class `P0`: PL/pgSQL error
	pub const PGSQL_PL_PGSQL_ERROR: Self = sqlstate![P 0];
	/// Postgres-specific: Class `XX`: Internal error
	pub const PGSQL_INTERNAL_ERROR: Self = sqlstate![X X];
}

/// # MySQL/MariaDB-specific class constants
impl Class {
	/// MySQL-specific: Class `70`: Interruption
	pub const MYSQL_INTERRUPTION: Self = sqlstate![7 0];
	/// MySQL-specific: Class `XA`: X/Open XA related error
	pub const MYSQL_XA_ERROR: Self = sqlstate![X A];
}

/// # ODBC-specific class constants
impl Class {
	/// ODBC-specific: Class `IM`: Driver error
	pub const ODBC_DRIVER_ERROR: Self = sqlstate![I M];
}

/// # Oracle-specific class constants
impl Class {
	/// Oracle-specific: Class `60`: System error
	pub const ORACLE_SYSTEM_ERROR: Self = sqlstate![6 0];
	/// Oracle-specific: Class `61`: Resource error
	pub const ORACLE_RESOURCE_ERROR: Self = sqlstate![6 1];
	/// Oracle-specific: Class `62`: Path name server and detached process error
	pub const ORACLE_PATH_NAME_SERVER_AND_DETACHED_PROCESS_ERROR: Self = sqlstate![6 2];
	/// Oracle-specific: Class `63`: Oracle*XA or two-task interface error
	pub const ORACLE_CA_OR_TWO_TASK_INTERFACE_ERROR: Self = sqlstate![6 3];
	/// Oracle-specific: Class `64`: control file, database file, redo file, archival or media recovery error
	pub const ORACLE_FILE_OR_MEDIA_RECOVERY_ERROR: Self = sqlstate![6 4];
	/// Oracle-specific: Class `65`: PL/SQL error
	pub const ORACLE_PL_SQL_ERROR: Self = sqlstate![6 5];
	/// Oracle-specific: Class `66`: SQL*Net driver error
	pub const ORACLE_SQL_NET_DRIVER_ERROR: Self = sqlstate![6 6];
	/// Oracle-specific: Class `67`: Licensing error
	pub const ORACLE_LICENSING_ERROR: Self = sqlstate![6 7];
	/// Oracle-specific: Class `69`: SQL*Connect error
	pub const ORACLE_SQL_CONNECT_ERROR: Self = sqlstate![6 9];
	/// Oracle-specific: Class `72`: SQL execute phase error
	pub const ORACLE_SQL_EXECUTE_PHASE_ERROR: Self = sqlstate![7 2];
	/// Oracle-specific: Class `82`: Internal error
	pub const ORACLE_INTERNAL_ERROR: Self = sqlstate![8 2];
	/// Oracle-specific: Class `90`: Debug event
	pub const ORACLE_DEBUG_EVENT: Self = sqlstate![9 0];
}

impl fmt::Debug for Class {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
		f.debug_tuple("Class").field(&&**self).finish()
	}
}

impl fmt::Display for Class {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
		f.write_str(self)
	}
}

impl AsRef<str> for Class {
	#[inline]
	fn as_ref(&self) -> &str {
		self
	}
}

impl AsRef<[u8]> for Class {
	#[inline]
	fn as_ref(&self) -> &[u8] {
		Char::array_as_bytes(&self.code)
	}
}

impl AsRef<[u8; 2]> for Class {
	#[inline]
	fn as_ref(&self) -> &[u8; 2] {
		Char::array_as_bytes(&self.code)
	}
}

impl From<Class> for [u8; 2] {
	#[inline]
	fn from(class: Class) -> Self {
		class.code.map(Char::as_byte)
	}
}

impl Deref for Class {
	type Target = str;
	#[inline]
	fn deref(&self) -> &Self::Target {
		Char::array_as_str(&self.code)
	}
}

impl PartialEq<str> for Class {
	fn eq(&self, other: &str) -> bool {
		&**self == other
	}
}

impl PartialEq<[u8]> for Class {
	fn eq(&self, other: &[u8]) -> bool {
		AsRef::<[u8]>::as_ref(self) == other
	}
}

impl FromStr for Class {
	type Err = ParseError;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		Self::from_bytes(s.as_bytes())
	}
}

impl TryFrom<&[u8]> for Class {
	type Error = ParseError;

	fn try_from(bytes: &[u8]) -> Result<Self, Self::Error> {
		Self::from_bytes(bytes)
	}
}

impl TryFrom<[u8; 2]> for Class {
	type Error = ParseError;

	fn try_from(bytes: [u8; 2]) -> Result<Self, Self::Error> {
		Self::from_byte_array(bytes)
	}
}

impl TryFrom<&str> for Class {
	type Error = ParseError;

	fn try_from(string: &str) -> Result<Self, Self::Error> {
		Self::from_str(string)
	}
}

#[cfg(feature = "serde")]
impl serde::Serialize for Class {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: serde::Serializer,
	{
		serializer.serialize_str(self)
	}
}

#[cfg(feature = "serde")]
impl<'de> serde::Deserialize<'de> for Class {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: serde::Deserializer<'de>,
	{
		deserializer
			.deserialize_str(crate::character::de::ArrayVisitor::new())
			.map(|code| Self { code })
	}
}

// Statically assert the intended memory layouts (2 bytes with multiple niches)
const _: () = assert!(core::mem::size_of::<Class>() == 2);
const _: () = assert!(core::mem::size_of::<Option<Class>>() == 2);
const _: () = assert!(core::mem::size_of::<Option<Option<Class>>>() == 2);
