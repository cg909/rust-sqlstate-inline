# sqlstate-inline - Efficient storage types for SQLSTATE code strings

[![Crates.io](https://img.shields.io/crates/v/sqlstate-inline.svg)](https://crates.io/crates/sqlstate-inline)
[![Documentation](https://docs.rs/sqlstate-inline/badge.svg)](https://docs.rs/sqlstate-inline/)
[![License](https://img.shields.io/crates/l/sqlstate-inline.svg)](https://www.mozilla.org/en-US/MPL/2.0/)
[![Build Status](https://gitlab.com/cg909/rust-sqlstate-inline/badges/master/pipeline.svg)](https://gitlab.com/cg909/rust-sqlstate-inline/-/commits/master)

## Usage

1. Add the following to your Cargo.toml:
```toml
[dependencies]
sqlstate-inline = "0.1"
```

2. Read the [crate documentation](https://docs.rs/sqlstate-inline/)

## Functionality

[SQLSTATE] codes are 5 character ASCII strings that only use the characters `A-Z` and `0-9`.
This crate provides an `SqlState` type that is const constructible and only consumes 5 bytes of
memory, providing niches, so that e.g. `Option<SqlState>` is also 5 bytes large. This type
dereferences to `str` and can be converted to e.g. `[u8; 5]` for maximum flexibility. E.g. using a
`std::string::String` instead allocates 5 bytes on the heap and use 24 bytes on the stack
(on 64-bit architectures) which in comparison is quite wasteful.

## Feature flags

- `std`: disable this feature to use the crate in `no_std` environments.
- `serde`: enable this feature to get `serde` (de-)serialization support for
  the provided data types.
 
## Supported Rust versions

The minimum supported Rust toolchain version is Rust **1.60.0**.

Currently tested up to version 1.66.0-nightly.

## Stability

This crate follows [semantic versioning](http://semver.org).

## License

Licensed under Mozilla Public License, Version 2.0 ([LICENSE](LICENSE)
or https://www.mozilla.org/en-US/MPL/2.0/).

### Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, shall be licensed as above
including compatibility with secondary licenses, as defined by the MPL.

[SQLSTATE]: https://en.wikipedia.org/wiki/SQLSTATE

<!--
© 2022 Christoph Grenz <https://grenz-bonn.de>
SPDX-License-Identifier: MPL-2.0
-->
